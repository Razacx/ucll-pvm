* Reasoning behind C/C++
  Single pass
  Declaration vs Definition
* Static allocation/stack/heap
* Compiling = producing code that behaves the same

C
* Preprocessor
* Pointers
  Output arguments
* malloc/free
* Arrays
* Strings in C
* assertions
* debug/release build


C++
* new/delete
* References
* Namespaces
* class/struct
  Destructors
  RAII
  Inheritance (public/private)
  Copy constructor
  Virtual
* Const (const int* const stuff)
* Smart pointers
* IO using standard streams
* Operator overloading
* Default argument values
* Templates
* STL
* nullptr
