// Receives Counter by value
void foo(Counter) { }

Counter c;

// Calls copy constructor
foo(c);
