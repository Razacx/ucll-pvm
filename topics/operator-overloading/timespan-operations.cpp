TimeSpan ts1, ts2;

// Possible operations
ts1 + ts2      ts1 - ts2
ts1 * 5        5 * ts1
ts1 / 2        -ts1

ts1 += ts2     ts1 -= ts2
ts1 *= 2       ts1 /= 2

ts1 == ts2     ts1 != ts2
ts1 < ts2      ts1 <= ts2
ts1 > ts2      ts1 >= ts2

ts1 = ts2
