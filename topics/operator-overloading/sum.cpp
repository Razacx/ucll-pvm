template<typename T>
T sum(const std::vector<T>& ns) {
  T init;

  for ( const T& n : ns ) {
    init += n;
  }

  return n;
}
