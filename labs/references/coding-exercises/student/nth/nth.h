#include <vector>

int& nth(std::vector<int>& ns, int index);

const int& nth(const std::vector<int>& ns, int index);
