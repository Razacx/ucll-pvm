#include "nth.h"

int& nth(std::vector<int>& ns, int index) {
	int actualIndex;
	if (index < 0) {
		actualIndex = ns.size() - (-(index) % (int) ns.size()) - 1;
	}
	else {
		actualIndex = index % ns.size();
	}
	return ns[actualIndex];
}

const int& nth(const std::vector<int>& ns, int index) {
	int actualIndex;
	if (index < 0) {
		actualIndex = ns.size() - (-(index) % (int) ns.size()) - 1;
	}
	else {
		actualIndex = index % ns.size();
	}
	return ns[actualIndex];
}